# Sleff

Calculate sleep efficiency

## Introduction

Your sleep efficiency is the ratio between the amount of time you are in bed and the amount of time you are actually asleep. There are watches that you can wear that measure this for you, but I can't sleep with one and my sleep therapist has told me that insomiacs such as me, who report in the morning when they fell asleep, when they woke up in the middle of the night, how long they lay awake and at what time they woke in the morning, are on average as accurate as people who keep record during the night, and sleep better. In the end, when you count the time you were in bed and the time you slept, you can calculate the efficiency or let this simple tool conveniently do that for you. It will also let you save your data to a cvs-file, so that over time you will gather insights.

## Usage

Start sleff using:

```
$ sleff.py [-h] [-o FILE] time_in_bed time_asleep
```

Use -h for a short help page.

## Copying

Sleff is licensed under the GNU Public License (GPL), version 3. This effectively means that you can freely change and redistribute sleff, but only with this exact license and all copyright messages intact.
