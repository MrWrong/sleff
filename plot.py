import sys
import matplotlib.pyplot as plt
import numpy as np

plt.style.use('_mpl-gallery')

# load data from stdin
x = []
y = []

for line in sys.stdin:
    x.append(line.split(',')[0])
    y.append(int(line.split(',')[1].rstrip()))

# plot:
plt.plot(x,y,label='Efficiency')
plt.legend(loc='best')
plt.show()
